/**
 * Задача 2.
 *
 * Добавьте роботу геттер и сеттер для приватного свойства energy.
 * Нужно, чтобы внешний код мог узнать заряд батареи робота.
 *
 * Условия:
 * - заданную форму конструктора включая его параметры менять нельзя — можно только дополнять;
 * - для отображения инфрмации о количестве энергии необходимо динамически создать элемент "p" с классом text-success;
 * - для отображения инфрмации об ошибке необходимо динамически создать элемент "p" с классом text-danger;
 * - все "p" элементы необходимо добавлять в "div" с классом messages.
 * 
 * Генерировать ошибки если:
 * - новый заряд батареи устанавливается в значении меньшем, чем 0;
 * - новый заряд батареи устанавливается в значении большем, чем значение MAX_ENERGY_CAPACITY;
 * - при создании экземпляра CleanerRobot изначальный уровень энергии зада в не рамок допустимого диапазона.
 * 
 * Приблизительный план действий:
 * 1. Добавить идентификатор в форму, который будет использоваться для добавления обработчика onsubmit
 * 2. Создать экземпляр CleanerRobot
 * 3. При сабмите формы устанавливать новое значение уровня энергии робота при помощи метода setEnergy
 * 4. При клике на элемент button читать значение уровня энергии робота и выводить его в p элемент с классом text-success
 * 5. Если в работе робота возникнут ошибки выводить их в p элемент с классом text-danger
 * 
 * Подсказки:
 * - в HTML допускается добавление идентификаторов для более удобной работы с дом
 * - вам могут потребоваться следующие методы и свойства — innerHTML, getElementById, createElement, onsubmit, onclick
 */

const div = document.querySelector('.messages');
const primary = document.querySelector('.btn-primary');
const info = document.querySelector('.btn-info');


function CleanerRobot(initialEnergy = 0 /* Изначальный заряд батареи */) {
    this.getEnergy = getEnergy;
    this.setEnergy = setEnergy;

    const MAX_ENERGY_CAPACITY = 100; /* Максимальная ёмкость батареи. */
    let energy = null;

    this.setEnergy(initialEnergy);

    function getEnergy() {
        // Реализация метода по чтению уровня заряда робота
        let out;
        if (this.energy >= 0 && this.energy <= MAX_ENERGY_CAPACITY) {
            out = `<p class="text-danger">Текущий заряд батареи: ${this.energy}<p/>`;
        }
        if (this.energy < 0) {
            out = `<p class="text-danger">Error: New energy level can not be less than 0.<p/>`;
        }
        if (this.energy > 100) {
            out = `<p class="text-danger">Error: New energy level can not be more than 100.<p/>`;
        }
        div.innerHTML = out;
    }
    function setEnergy(value) {
        // Реализация метода по установке уровня заряда робота
        this.energy = value;
    }
}

const cleanerRobot = new CleanerRobot(22);

// Ниже необходимо написать логику работы с DOM


document.querySelector('.form-control').value;


primary.addEventListener('click', (e) => {
    e.preventDefault();
    cleanerRobot.getEnergy();
});


info.addEventListener('click', (e) => {
    e.preventDefault();
    cleanerRobot.setEnergy(+document.querySelector('.form-control').value);
});


/* Текущий заряд батареи: 22 */
/* Информацию необходимо вывести в p элемент с классом text-success */
// console.log(`Текущий заряд батареи: ${cleanerRobot.getEnergy()}`);
// ответ закоментирован ниже
cleanerRobot.setEnergy(-22);
cleanerRobot.getEnergy();

/* Этот код необходимо вызвать при сабмите формы */
// cleanerRobot.setEnergy(55);

/* Текущий заряд батареи: 55 */
/* Информацию необходимо вывести в p элемент с классом text-success */
// console.log(`Текущий заряд батареи: ${cleanerRobot.getEnergy()}`);
// ответ закоментирован ниже
// cleanerRobot.setEnergy(55);
// cleanerRobot.getEnergy();



try {
    new CleanerRobot(-1);
} catch (error) {
    /* Error: New energy level can not be less than 0. */
    /* Информацию необходимо вывести в p элемент с классом text-danger */
    cleanerRobot.getEnergy();
    console.log(`${error.name}: ${error.message}`);
}

try {
    cleanerRobot.setEnergy(-22);
} catch (error) {
    /* Error: New energy level can not be less than 0. */
    /* Информацию необходимо вывести в p элемент с классом text-danger */
    cleanerRobot.getEnergy();
    console.log(`${error.name}: ${error.message}`);
}

try {
    cleanerRobot.setEnergy(101);
} catch (error) {
    /* Error: New energy level can not be more than 100. */
    /* Информацию необходимо вывести в p элемент с классом text-danger */
    cleanerRobot.getEnergy();
    console.log(`${error.name}: ${error.message}`);
}
